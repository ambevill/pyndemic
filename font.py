FONTS = {
    '12b': ('Arial', '8', 'bold'),
    16: ('Arial', '12'),
    '16b': ('Arial', '12', 'bold'),
    18: ('Arial', '14'),
    '18b': ('Arial', '14', 'bold'),
    20: ('Arial', '16'),
    '20b': ('Arial', '16', 'bold'),
    '20i': ('Arial', '16', 'italic'),
}
