def hexcolor(r=0, g=0, b=0):
    assert 0 <= r < 256
    assert 0 <= g < 256
    assert 0 <= b < 256
    return '#{:0>2}{:0>2}{:0>2}'.format(hex(r)[2:], hex(g)[2:], hex(b)[2:])


COLOR_DICT = dict(
    r='red',
    b='blue',
    y=hexcolor(120, 120, 0),
    k='black',
    c='cyan',
    m='magenta',
    o=hexcolor(255, 100, 0),
    n=hexcolor(100, 50, 0),
    g=hexcolor(0, 250, 0),
    d=hexcolor(0, 120, 0),
    w='white',
)
