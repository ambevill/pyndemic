import tkinter as tk


class Erasable(object):
    def __init__(self):
        self.erasable_widgets = []

    def erasable(self, widget_id):
        self.erasable_widgets.append(widget_id)

    def erase(self, canvas):
        while len(self.erasable_widgets) > 0:
            canvas.delete(self.erasable_widgets.pop())
