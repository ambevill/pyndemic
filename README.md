Pyndemic
========

Pyndemic is a Python3 virtual replacement for the physical gamepieces of a similarly titled board game. It is intended to be a convenient complement to the physical game, e.g. automatically shuffling and sorting the cards and placing infections. It is not intended to replace the title or infringe on its intellectual property.

Instructions are deliberately omitted. New users are encouraged to consult the rule book in their copy of the original game. Specific click sequences of the Pyndemic UI (e.g. drawing from the bottom of a deck) can be looked up in state.py State.drag_callback().