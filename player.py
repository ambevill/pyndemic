import random
#
from clickable import Clickable
from erasable import Erasable
from color import COLOR_DICT
from font import FONTS
from deck import Deck
from card import CARD_WIDTH, CARD_HEIGHT


class Player(Clickable, Erasable):
    def __init__(self, name, color, bcolor, city):
        Clickable.__init__(self)
        Erasable.__init__(self)
        self.name = name
        self.color = color
        self.bcolor = bcolor
        self.city = city
        self.cards = Deck(7, self.name + "'s Hand")
        self.active = False

    def draw_static(self, canvas, x, y):
        canvas.create_rectangle(
            x, y,
            x + CARD_WIDTH, y + CARD_HEIGHT - 1,
            fill=COLOR_DICT[self.color],
            outline=COLOR_DICT[self.bcolor],
            width=1)
        canvas.create_text(
            x + CARD_WIDTH // 2, y + CARD_HEIGHT // 2,
            font=FONTS[16],
            fill=COLOR_DICT[self.bcolor],
            text=self.name)

    def draw_dynamic(self, canvas, active=False):
        self.erase(canvas)
        x, y = self.city.get_player_xy()
        #
        text_item = canvas.create_text(
            x, y,
            font=FONTS[16],
            fill=COLOR_DICT[self.bcolor],
            text=self.name[0])
        bbox = canvas.bbox(text_item)
        canvas.delete(text_item)
        self.erasable(canvas.create_rectangle(
            *bbox,
            fill=COLOR_DICT[self.color],
            outline=COLOR_DICT[self.bcolor],
            dash=(3, 3) if active else None,
            width=2 if active else 1))
        self.erasable(canvas.create_text(
            x, y,
            font=FONTS['16b'] if active else FONTS[16],
            fill=COLOR_DICT[self.bcolor],
            text=self.name[0]))
        #
        self.register_clickable(
            bbox[0], bbox[1],
            bbox[2] - bbox[0], bbox[3] - bbox[1])

    def __str__(self):
        return self.name

    @staticmethod
    def init_all(num_players, cities):
        return random.sample((
                Player('Contingency', 'c', 'k', cities[0]),
                Player('Dispatcher',  'm', 'k', cities[0]),
                Player('Medic',       'o', 'k', cities[0]),
                Player('Operations',  'g', 'k', cities[0]),
                Player('Quarantine',  'd', 'w', cities[0]),
                Player('Researcher',  'n', 'w', cities[0]),
                Player('Scientist',   'w', 'k', cities[0])),
            num_players)