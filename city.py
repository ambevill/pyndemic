from font import FONTS
from color import COLOR_DICT
from clickable import Clickable
from erasable import Erasable


class City(Clickable, Erasable):
    RADIUS = 15

    def __init__(self, name, x, y, color, research_center = False):
        Clickable.__init__(self)
        Erasable.__init__(self)
        self.name = name
        self.xy = x, y
        self.color = color
        self.neighbors = []
        self.infection_dict = dict(
            b=0,
            y=0,
            k=0,
            r=0,)
        self.foreign_infection_dict = dict(
            b=ForeignInfection(self, 'b'),
            y=ForeignInfection(self, 'y'),
            k=ForeignInfection(self, 'k'),
            r=ForeignInfection(self, 'r'),)
        self.player_counter = 0
        self.research_center = research_center

    def infect(self, num_infections=1, color=None):
        if color is None:
            color = self.color
        self.infection_dict[color] += num_infections
        if self.infection_dict[color] > 3:
            self.infection_dict[color] = 3
            return list(self.neighbors)
        return None

    def heal(self, num_heal=1, color=None):
        if color is None:
            color = self.color
        self.infection_dict[color] -= num_heal
        self.infection_dict[color] = max(self.infection_dict[color], 0)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def draw_static(self, canvas):
        canvas.create_oval(
            self.xy[0] - self.RADIUS, self.xy[1] - self.RADIUS,
            self.xy[0] + self.RADIUS, self.xy[1] + self.RADIUS,
            fill=COLOR_DICT['w'],
            outline=COLOR_DICT[self.color],
            width=3)
        canvas.create_text(
            self.xy[0], self.xy[1] + 27,
            fill=COLOR_DICT['k'],
            font=FONTS[16],
            text=self.name)
        self.register_clickable(
            self.xy[0] - self.RADIUS,
            self.xy[1] - self.RADIUS,
            self.RADIUS * 2,
            self.RADIUS * 2)

    def draw_dynamic(self, canvas):
        self.erase(canvas)
        # centered roman numerals for infection cubes
        infection = self.infection_dict[self.color]
        self.erasable(canvas.create_text(
            self.xy[0], self.xy[1],
            fill=COLOR_DICT[self.color],
            font=FONTS['20b'],
            text=infection * 'I'))
        #
        # off-color infections
        x = self.xy[0]
        y = self.xy[1] - self.RADIUS - 10
        for color in 'bykr':
            if color == self.color:
                continue
            infection = self.infection_dict[color]
            if infection > 0:
                self.foreign_infection_dict[color].draw(
                    canvas, infection, x, y)
                y -= 16
            else:
                self.foreign_infection_dict[color].deregister_clickable()
                self.foreign_infection_dict[color].erase(canvas)
        #
        # research center
        if self.research_center:
            side = 16
            offset = 18
            xl, xc, xr = self.xy[0] - offset - side, self.xy[0] - offset - side // 2, self.xy[0] - offset
            yl, yc, yh = self.xy[1] + side // 2, self.xy[1], self.xy[1] - side // 2
            self.erasable(canvas.create_polygon(
                ((xl, yl), (xl, yc), (xc, yh), (xr, yc), (xr, yl)),
                fill=COLOR_DICT['w'],
                outline=None))
        #
        # reset the player counter
        self.player_counter = 0

    def get_player_xy(self):
        self.player_counter += 1
        x = self.xy[0] + 10 + self.player_counter * 18
        y = self.xy[1]
        return (x, y)

    @staticmethod
    def init_all():
        return (
            # North America
            City('Atlanta', 202, 163, 'b', research_center = True),
            City('San Francisco', 59, 128, 'b'),
            City('Los Angeles', 71, 216, 'y'),
            City('Mexico City', 153, 247, 'y'),
            City('Miami', 260, 233, 'y'),
            City('Washington', 300, 160, 'b'),
            City('New York', 330, 108, 'b'),
            City('Montreal', 262, 97, 'b'),
            City('Chicago', 172, 99, 'b'),
            # South America
            City('Bogota', 249, 323, 'y'),
            City('Lima', 215, 418, 'y'),
            City('Santiago', 226, 513, 'y'),
            City('Buenos Aires', 322, 497, 'y'),
            City('Sao Paulo', 374, 431, 'y'),
            # Africa
            City('Algiers', 572, 193, 'k'),
            City('Lagos', 545, 309, 'y'),
            City('Kinshasa', 598, 369, 'y'),
            City('Johannesburg', 645, 454, 'y'),
            City('Khartoum', 653, 293, 'y'),
            City('Cairo', 636, 210, 'k'),
            # Middle East
            City('Baghdad', 708, 182, 'k'),
            City('Riyadh', 719, 257, 'k'),
            City('Mumbai', 798, 271, 'k'),
            City('Chennai', 863, 317, 'k'),
            City('Kolkata', 909, 207, 'k'),
            City('Delhi', 851, 188, 'k'),
            City('Karachi', 788, 210, 'k'),
            City('Tehran', 773, 129, 'k'),
            # Far East
            City('Jakarta', 921, 394, 'r'),
            City('Sydney', 1107, 510, 'r'),
            City('Manila', 1059, 334, 'r'),
            City('Ho Chi Minh', 976, 339, 'r'),
            City('Taipei', 1041, 231, 'r'),
            City('Osaka', 1102, 204, 'r'),
            City('Tokyo', 1093, 141, 'r'),
            City('Seoul', 1034, 107, 'r'),
            City('Beijing', 959, 113, 'r'),
            City('Shanghai', 963, 170, 'r'),
            City('Hong Kong', 972, 241, 'r'),
            City('Bangkok', 923, 277, 'r'),
            # Europe
            City('London', 489, 61, 'b'),
            City('Madrid', 479, 144, 'b'),
            City('Paris', 554, 104, 'b'),
            City('Milan', 611, 88, 'b'),
            City('Istanbul', 648, 137, 'k'),
            City('Moscow', 716, 89, 'k'),
            City('St. Petersburg', 670, 31, 'b'),
            City('Essen', 576, 48, 'b'))


class Edge(object):
    def __init__(self, city1, city2, wrap=False):
        self.cities = city1, city2
        assert city1 not in city2.neighbors, '{} {}'.format(city1, city2)
        assert city2 not in city1.neighbors, '{} {}'.format(city1, city2)
        city1.neighbors.append(city2)
        city2.neighbors.append(city1)
        self.wrap = wrap

    @staticmethod
    def from_names(all_cities, name1, name2, wrap=False):
        cities = []
        for city in all_cities:
            if city.name in (name1, name2):
                cities.append(city)
        errmsg = 'Edge.from_names(..., {}, {}): Incorrect num matches {}.'
        errmsg = errmsg.format(name1, name2, cities)
        assert len(cities) == 2, errmsg
        return Edge(cities[0], cities[1], wrap)

    def draw_static(self, canvas):
        if self.wrap:
            width = canvas.config()['width']
            if self.cities[0].xy[0] > self.cities[1].xy[0]:
                width *= -1
            canvas.create_line(
                self.cities[0].xy[0], self.cities[0].xy[1],
                self.cities[1].xy[0] - 1200, self.cities[1].xy[1],
                fill='white',
                width=1)
            canvas.create_line(
                self.cities[0].xy[0] + 1200, self.cities[0].xy[1],
                self.cities[1].xy[0], self.cities[1].xy[1],
                fill='white',
                width=1)
        else:
            canvas.create_line(
                self.cities[0].xy[0], self.cities[0].xy[1],
                self.cities[1].xy[0], self.cities[1].xy[1],
                fill='white',
                width=1)

    @staticmethod
    def init_all(cities):
        return (
            Edge.from_names(cities, 'Atlanta', 'Miami'),
            Edge.from_names(cities, 'Atlanta', 'Washington'),
            Edge.from_names(cities, 'Atlanta', 'Chicago'),
            Edge.from_names(cities, 'San Francisco', 'Los Angeles'),
            Edge.from_names(cities, 'San Francisco', 'Chicago'),
            Edge.from_names(cities, 'San Francisco', 'Tokyo', True),
            Edge.from_names(cities, 'San Francisco', 'Manila', True),
            Edge.from_names(cities, 'Los Angeles', 'Sydney', True),
            Edge.from_names(cities, 'Los Angeles', 'Mexico City'),
            Edge.from_names(cities, 'Los Angeles', 'Chicago'),
            Edge.from_names(cities, 'Mexico City', 'Lima'),
            Edge.from_names(cities, 'Mexico City', 'Bogota'),
            Edge.from_names(cities, 'Mexico City', 'Miami'),
            Edge.from_names(cities, 'Mexico City', 'Chicago'),
            Edge.from_names(cities, 'Miami', 'Bogota'),
            Edge.from_names(cities, 'Miami', 'Washington'),
            Edge.from_names(cities, 'Washington', 'New York'),
            Edge.from_names(cities, 'Washington', 'Montreal'),
            Edge.from_names(cities, 'New York', 'Madrid'),
            Edge.from_names(cities, 'New York', 'London'),
            Edge.from_names(cities, 'New York', 'Montreal'),
            Edge.from_names(cities, 'Montreal', 'Chicago'),
            # South America
            Edge.from_names(cities, 'Bogota', 'Lima'),
            Edge.from_names(cities, 'Bogota', 'Buenos Aires'),
            Edge.from_names(cities, 'Bogota', 'Sao Paulo'),
            Edge.from_names(cities, 'Lima', 'Santiago'),
            Edge.from_names(cities, 'Buenos Aires', 'Sao Paulo'),
            Edge.from_names(cities, 'Sao Paulo', 'Lagos'),
            Edge.from_names(cities, 'Sao Paulo', 'Madrid'),
            # Africa
            Edge.from_names(cities, 'Algiers', 'Madrid'),
            Edge.from_names(cities, 'Algiers', 'Paris'),
            Edge.from_names(cities, 'Algiers', 'Istanbul'),
            Edge.from_names(cities, 'Algiers', 'Cairo'),
            Edge.from_names(cities, 'Lagos', 'Kinshasa'),
            Edge.from_names(cities, 'Lagos', 'Khartoum'),
            Edge.from_names(cities, 'Kinshasa', 'Khartoum'),
            Edge.from_names(cities, 'Kinshasa', 'Johannesburg'),
            Edge.from_names(cities, 'Johannesburg', 'Khartoum'),
            Edge.from_names(cities, 'Khartoum', 'Cairo'),
            Edge.from_names(cities, 'Cairo', 'Riyadh'),
            Edge.from_names(cities, 'Cairo', 'Baghdad'),
            Edge.from_names(cities, 'Cairo', 'Istanbul'),
            # Middle East
            Edge.from_names(cities, 'Baghdad', 'Riyadh'),
            Edge.from_names(cities, 'Baghdad', 'Karachi'),
            Edge.from_names(cities, 'Baghdad', 'Tehran'),
            Edge.from_names(cities, 'Baghdad', 'Istanbul'),
            Edge.from_names(cities, 'Riyadh', 'Karachi'),
            Edge.from_names(cities, 'Mumbai', 'Karachi'),
            Edge.from_names(cities, 'Mumbai', 'Delhi'),
            Edge.from_names(cities, 'Mumbai', 'Chennai'),
            Edge.from_names(cities, 'Chennai', 'Delhi'),
            Edge.from_names(cities, 'Chennai', 'Kolkata'),
            Edge.from_names(cities, 'Chennai', 'Bangkok'),
            Edge.from_names(cities, 'Chennai', 'Jakarta'),
            Edge.from_names(cities, 'Kolkata', 'Bangkok'),
            Edge.from_names(cities, 'Kolkata', 'Hong Kong'),
            Edge.from_names(cities, 'Kolkata', 'Delhi'),
            Edge.from_names(cities, 'Delhi', 'Karachi'),
            Edge.from_names(cities, 'Delhi', 'Tehran'),
            Edge.from_names(cities, 'Karachi', 'Tehran'),
            Edge.from_names(cities, 'Tehran', 'Moscow'),
            # Far East
            Edge.from_names(cities, 'Jakarta', 'Bangkok'),
            Edge.from_names(cities, 'Jakarta', 'Ho Chi Minh'),
            Edge.from_names(cities, 'Jakarta', 'Sydney'),
            Edge.from_names(cities, 'Sydney', 'Manila'),
            Edge.from_names(cities, 'Manila', 'Ho Chi Minh'),
            Edge.from_names(cities, 'Manila', 'Hong Kong'),
            Edge.from_names(cities, 'Manila', 'Taipei'),
            Edge.from_names(cities, 'Ho Chi Minh', 'Hong Kong'),
            Edge.from_names(cities, 'Ho Chi Minh', 'Bangkok'),
            Edge.from_names(cities, 'Taipei', 'Hong Kong'),
            Edge.from_names(cities, 'Taipei', 'Shanghai'),
            Edge.from_names(cities, 'Taipei', 'Osaka'),
            Edge.from_names(cities, 'Osaka', 'Tokyo'),
            Edge.from_names(cities, 'Tokyo', 'Shanghai'),
            Edge.from_names(cities, 'Tokyo', 'Seoul'),
            Edge.from_names(cities, 'Seoul', 'Beijing'),
            Edge.from_names(cities, 'Seoul', 'Shanghai'),
            Edge.from_names(cities, 'Beijing', 'Shanghai'),
            Edge.from_names(cities, 'Shanghai', 'Hong Kong'),
            Edge.from_names(cities, 'Hong Kong', 'Bangkok'),
            # Europe
            Edge.from_names(cities, 'London', 'Madrid'),
            Edge.from_names(cities, 'London', 'Paris'),
            Edge.from_names(cities, 'London', 'Essen'),
            Edge.from_names(cities, 'Madrid', 'Paris'),
            Edge.from_names(cities, 'Paris', 'Milan'),
            Edge.from_names(cities, 'Paris', 'Essen'),
            Edge.from_names(cities, 'Milan', 'Essen'),
            Edge.from_names(cities, 'Milan', 'Istanbul'),
            Edge.from_names(cities, 'Istanbul', 'Moscow'),
            Edge.from_names(cities, 'Istanbul', 'St. Petersburg'),
            Edge.from_names(cities, 'Moscow', 'St. Petersburg'),
            Edge.from_names(cities, 'St. Petersburg', 'Essen'))


class ForeignInfection(Clickable, Erasable):
    def __init__(self, city, color):
        Clickable.__init__(self)
        Erasable.__init__(self)
        self.city = city
        self.color = color

    def draw(self, canvas, infection, x, y):
        self.erase(canvas)
        self.erasable(canvas.create_rectangle(
            x - 7, y - 7,
            x + 7, y + 7,
            fill=COLOR_DICT['w'],
            outline=COLOR_DICT[self.color],
            width=1))
        self.erasable(canvas.create_text(
            x, y,
            fill=COLOR_DICT[self.color],
            font=FONTS['12b'],
            text=infection * 'I'))
        self.register_clickable(x - 7, y - 7, 14, 14)
