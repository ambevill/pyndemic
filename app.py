import tkinter as tk
#
from color import COLOR_DICT
from output import debug
from state import State


class App(tk.Tk):
    def __init__(self, num_players = 2, num_epidemics = 5, pickle_path = None):
        tk.Tk.__init__(self)
        #
        self.wm_title('Pyndemic')
        #~ self.overrideredirect(True)  # hides top bar, disables keybindings
        #~ self.attributes('-topmost', True)  # keep app on top
        self.bind('<Control-q>', self.destroy)
        self.bind('<Escape>', self.iconify)
        #
        self.canvas = tk.Canvas(
            self,
            width=1200, height=700,
            background=COLOR_DICT['d'])
        self.canvas.pack()
        #
        if pickle_path is None:
            self.state = State(num_players=num_players,
                               num_epidemics=num_epidemics)
        else:
            self.state = State.from_pickle(pickle_path)
        self.state.draw_static(self.canvas)
        self.state.draw_dynamic(self.canvas)
        self.state.to_pickle('pyndemic-logs/state.pickle')
        #
        self.canvas.bind('<ButtonPress-1>', self.callback)
        self.canvas.bind('<ButtonRelease-1>', self.callback)
        self.canvas.bind('<ButtonPress-3>', self.callback)
        self.canvas.bind('<ButtonRelease-3>', self.callback)

    def callback(self, event):
        debug('on_event:', event, event.type)
        if event.type == '4':
            self.last_buttonpress = event
        elif event.type == '5':
            self.state.drag_callback(self.last_buttonpress, event)
            self.last_buttonpress = None
        else:
            raise AssertionError('Unknown event type {}'.format(event.__dict__))
        self.state.to_pickle('pyndemic-logs/state.pickle')
        self.state.draw_dynamic(self.canvas)

    def destroy(self, event=None):
        debug('Destroy called')
        tk.Tk.destroy(self)

    def iconify(self, event=None):
        debug('Withdraw called')
        tk.Tk.iconify(self)
