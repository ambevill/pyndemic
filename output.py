import os
try: import pyttsx3
except ImportError: tts_engine = None
else: tts_engine = pyttsx3.init()


try:
    os.mkdir('pyndemic-logs')
except OSError:
    pass
debug_file = open('pyndemic-logs/debug.txt', 'w', 1)  # line-buffered
info_file = open('pyndemic-logs/info.txt', 'w', 1)  # line-buffered

def debug(*args):
    s = ' '.join(str(a) for a in args) + '\n'
    debug_file.write(s)

def info(*args):
    s = ' '.join(str(a) for a in args) + '\n'
    debug_file.write(s)
    info_file.write(s)
    print(s[:-1])
    if tts_engine is not None:
        tts_engine.say(s)
        tts_engine.runAndWait()
