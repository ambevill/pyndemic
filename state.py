import math
import random
import pickle
#
from output import info, debug
from clickable import Clickable
from city import City, Edge, ForeignInfection
from player import Player
from card import Card, PlayerCard, InfectionCard
from deck import Deck
from counter import CubeCounter, RateCounter, OutbreakCounter


class State(object):
    def __init__(self, num_players=2, num_epidemics=5):
        Clickable.all_clickables = set()
        Deck.all_decks = set()
        #
        self.cube_counters = (
            CubeCounter('b'),
            CubeCounter('y'),
            CubeCounter('k', bcolor='w'),
            CubeCounter('r'),
            CubeCounter('w', total=6),
        )
        self.cube_counters_dict = dict()
        for cc in self.cube_counters:
            self.cube_counters_dict[cc.color] = cc
        self.rate_counter = RateCounter()
        self.outbreak_counter = OutbreakCounter()
        #
        # initialize the map with no infections and one research center
        self.cities = City.init_all()
        self.edges = Edge.init_all(self.cities)
        #
        # initialize each player with a role but no cards
        self.players = Player.init_all(num_players, self.cities)
        self.active_player_index = 0
        #
        # shuffle city and event PlayerCards
        self.player_draw = Deck(5, 'Player Draw', face_up=False, color='b')
        self.player_discard = Deck(5, 'Player Discard')
        for city in self.cities:
            self.player_draw.insert_bottom(PlayerCard(city.name, city.color))
        self.player_draw.insert_bottom((
            PlayerCard('One Quiet Night', 'm'),
            PlayerCard('Airlift', 'm'),
            PlayerCard('Government Grant', 'm'),
            PlayerCard('Resilient Population', 'm'),
            PlayerCard('Forecast', 'm'),
        ))
        self.player_draw.shuffle()
        # deal to players
        num_cards = max(6 - num_players, 2)
        for player in self.players:
            for i in range(num_cards):
                player.cards.insert_bottom(self.player_draw.take_top())
        # insert epidemics
        info('Inserting {} epidemic cards'.format(num_epidemics))
        a, b = 0, 0
        insert_indices = []
        for i in range(num_epidemics):
            a = b
            b = math.ceil(len(self.player_draw) * (i + 1.) / num_epidemics)
            insert_indices.append(random.randrange(a, b))
        for i in insert_indices[::-1]:
            self.player_draw.insert(i, PlayerCard('Epidemic', 'g'))
        debug('Final player draw:', self.player_draw.cards)
        #
        # shuffle the infection deck
        self.infection_draw = Deck(
            5, 'Infection Draw', face_up=False, color='g')
        self.infection_discard = Deck(5, 'Infection Discard')
        for city in self.cities:
            self.infection_draw.insert_bottom(InfectionCard(city))
        self.infection_draw.shuffle()
        for i in (3, 2, 1):
            for j in range(3):
                card = self.infection_draw.take_top()
                city = card.city
                city.infect(i)
                self.infection_discard.insert_top(card)
        #
        # add a spare discard for weird events or whatever
        self.misc_discard = Deck(10, 'Misc Discard')

    def to_pickle(self, filename):
        with open(filename, 'wb') as outfile:
            pickle.dump(self, outfile)

    @staticmethod
    def from_pickle(filename):
        with open(filename, 'rb') as infile:
            state = pickle.load(infile)
        assert state.players[0].city in state.cities
        return state

    def draw_static(self, canvas):
        # the geography
        for edge in self.edges:
            edge.draw_static(canvas)
        for city in self.cities:
            city.draw_static(canvas)
        #
        # the player cards
        #~ image = tk.PhotoImage(file='images/card/player.png')
        #~ canvas.create_image((10, 575), image)
        self.player_draw.draw_static(canvas, 10, 565)
        self.player_discard.draw_static(canvas, 110, 565)
        #
        # the infection cards
        #~ image = tk.PhotoImage(file='images/card/infection.png')
        #~ canvas.create_image((225, 565), image)
        self.infection_draw.draw_static(canvas, 225, 565)
        self.infection_discard.draw_static(canvas, 325, 565)
        #
        # the misc discard
        self.misc_discard.draw_static(canvas, 10, 350)
        #
        # the players' hands
        for i, player in enumerate(self.players):
            player.cards.draw_static(canvas, 445 + 101 * i, 518)
            player.draw_static(canvas, 445 + 101 * i, 500)
        #
        # the counters
        x = 10
        for i in range(5):
            self.cube_counters[i].draw_static(canvas, x, 35)
            x += 24
        self.rate_counter.draw_static(canvas, 265, 555)
        self.outbreak_counter.draw_static(canvas, 10, 10)

    def draw_dynamic(self, canvas):
        for city in self.cities:
            city.draw_dynamic(canvas)
        for (i, player) in enumerate(self.players):
            flag = i == self.active_player_index
            player.draw_dynamic(canvas, flag)
        #
        self.player_draw.draw_dynamic(canvas, 10, 565)
        self.player_discard.draw_dynamic(canvas, 110, 565)
        self.infection_draw.draw_dynamic(canvas, 225, 565)
        self.infection_discard.draw_dynamic(canvas, 325, 565)
        self.misc_discard.draw_dynamic(canvas, 10, 350)
        #
        # the players' hands
        for i, player in enumerate(self.players):
            player.cards.draw_dynamic(canvas, 445 + 100 * i, 518)
        #
        # the cube counters
        cube_counts = dict(
            b=0,
            y=0,
            k=0,
            r=0,
            w=0,
        )
        for city in self.cities:
            for color in 'bykr':
                cube_counts[color] += city.infection_dict[color]
            if city.research_center:
                cube_counts['w'] += 1
        x = 10
        for cube_counter in self.cube_counters:
            cube_counter.draw_dynamic(canvas, x, 35, cube_counts)
            x += 24
        #
        # the other counters
        self.rate_counter.draw_dynamic(canvas, 260, 547)
        self.outbreak_counter.draw_dynamic(canvas, 10, 10)

    def drag_callback(self, down_event, up_event):
        c1 = Clickable.identify_click(down_event)
        c2 = Clickable.identify_click(up_event)
        button = down_event.num
        button1 = button == 1
        button3 = button == 3
        assert button in (1, 3), 'Unrecognized button {}'.format(button)
        debug('drag from {} to {} with button {}'.format(c1, c2, button))
        #
        if c1 is c2:
            if isinstance(c1, City):
                if button1:
                    info('Manually infecting', c1)
                    outbreak_into = c1.infect()
                    if outbreak_into is not None:
                        info('Ignoring outbreak into', outbreak_into)
                elif button3:
                    color = c1.color
                    num_heal = 3 if self.cube_counters_dict[c1.color].status >= CubeCounter.CURED else 1
                    info(f'Healing {num_heal} from {c1}')
                    c1.heal(num_heal)
            elif isinstance(c1, ForeignInfection):
                color = c1.color
                city = c1.city
                if button1:
                    info(f'Manually infecting {city} with {color}')
                    city.infect(color=color)
                elif button3:
                    num_heal = 3 if self.cube_counters_dict[color].status >= CubeCounter.CURED else 1
                    info(f'Healing {num_heal} {color} from {city}')
                    city.heal(color=color)
            elif isinstance(c1, CubeCounter):
                if button1:
                    c1.increment()
                    info(f'Disease {c1.color} is now {c1.status}')
                elif button3:
                    c1.increment(better=False)
                    info(f'Disease {c1.color} is now {c1.status}')
            elif isinstance(c1, OutbreakCounter):
                if button1:
                    info('Manually incrementing outbreak counter')
                    self.outbreak_counter.increment()
                elif button3:
                    info('Manually decrementing outbreak counter')
                    self.outbreak_counter.increment(-1)
            elif isinstance(c1, RateCounter):
                if button1:
                    info('Manually incrementing rate counter')
                    self.rate_counter.increment()
                elif button3:
                    info('Manually decrementing rate counter')
                    self.rate_counter.increment(-1)
            elif isinstance(c1, Player):
                info('Manually activating player', c1)
                self.active_player_index = self.players.index(c1)
            elif c1 is self.player_draw:
                player = self.players[self.active_player_index]
                card1 = self.player_draw.take_top()
                player.cards.insert_top(card1)
                try:
                    card2 = self.player_draw.take_top()
                except IndexError:
                    card2 = None
                else:
                    player.cards.insert_top(card2)
                info(f'{player} draws {card1}, {card2}')
            elif c1 is self.infection_draw:
                # left click to draw n from top
                if down_event.num == 1:
                    self.draw_infection(num_cities = self.rate_counter.get_current_rate())
                    # next player's turn
                    self.active_player_index = (self.active_player_index + 1) % len(self.players)
                    info(f"It is the {self.players[self.active_player_index].name}'s turn")
                # right click to draw 1 from bottom
                elif down_event.num == 3:
                    self.draw_infection(take_from = 'bottom', num_infect = 3)
        elif c1 is self.infection_discard and c2 is self.infection_draw:
            info('Shuffling discard and placing on top of infection draw')
            self.infection_discard.shuffle()
            self.infection_draw.insert_top(self.infection_discard.take_all())
        elif (  isinstance(c1, PlayerCard) and
                c2 is self.player_discard and
                button3):
            self.attempt_cure(c1)
        elif isinstance(c1, Card) and isinstance(c2, Deck):
            info('Moving {} to {}'.format(c1, c2))
            self.move_card(card=c1, deck=c2)
        elif isinstance(c1, Deck) and isinstance(c2, Deck):
            info('Moving {} from {} to {}'.format(
                c1.cards[0], c1, c2))
            try:
                c2.insert_top(c1.take_top())
            except IndexError:
                info('Deck {} is empty, no action.'.format(c1))
        elif isinstance(c1, Player) and isinstance(c2, City):
            info('{} moves to {}'.format(c1, c2))
            c1.city = c2
        elif c1 is self.cube_counters_dict['w'] and isinstance(c2, City):
            info('Building research center at', c2)
            c2.research_center = True
        elif isinstance(c1, City) and c2 is self.cube_counters_dict['w']:
            info('Withdrawing research center at city', c1)
            c1.research_center = False

    def draw_infection(self, num_cities = 1, take_from = 'top', num_infect = 1):

        if take_from == 'top':
            take = self.infection_draw.take_top
        else:
            info('Drawing from the bottom of the infection draw')
            take = self.infection_draw.take_bottom

        for i in range(num_cities):
            try:
                card = take()
            except IndexError:
                info('Out of cards')
                return
            self.infection_discard.insert_top(card)
            #
            color = card.city.color
            if self.cube_counters_dict[color].status >= CubeCounter.ERADICATED:
                info(f'Cannot infect {card.city} because {color} is eradicated.')
                continue
            #
            info('Infecting', card.city, 'with', num_infect)
            outbreaks_to = card.city.infect(num_infect)
            #
            if outbreaks_to is not None:
                info('Outbreak from {} to {}'.format(card.city, outbreaks_to))
                outbreaks_from = [card.city]
                self.outbreak_counter.increment()
                while len(outbreaks_to) > 0:
                    city = outbreaks_to.pop(0)
                    if city in outbreaks_from:
                        info('{} cannot re-outbreak'.format(city))
                        continue
                    new_outbreaks_to = city.infect(color=color)
                    if new_outbreaks_to is not None:
                        info('Outbreak from {} to {}'.format(
                            city, new_outbreaks_to))
                        outbreaks_from.append(city)
                        self.outbreak_counter.increment()
                        outbreaks_to.extend(new_outbreaks_to)

    def attempt_cure(self, card):
        player = self.players[self.active_player_index]
        color = card.color
        info("Attempting to cure {} from {}'s hand".format(color, player))
        #
        num_cards = 5
        if player.name == 'Scientist':
            num_cards = 4
        try:
            cards = player.cards.take_by_color(color, num_cards)
        except AssertionError:
            info("Unable to find {} {} cards in {}'s hand".format(
                num_cards, color, player))
            return
        self.player_discard.insert_top(cards)
        self.cube_counters_dict[color].status = CubeCounter.CURED
        info(f"Cured {color}")

    def move_card(self, card, deck):
        # remove the card from all old decks
        Deck.withdraw_card(card)
        # insert the card in the new deck
        deck.insert_top(card)
