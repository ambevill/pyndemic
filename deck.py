import tkinter as tk
import random
#
from output import debug
from color import COLOR_DICT
from font import FONTS
from clickable import Clickable
from erasable import Erasable
from card import CARD_WIDTH, CARD_HEIGHT


class Deck(Clickable, Erasable):
    all_decks = set()

    def __init__(self, height, name, face_up=True, color=None):
        Clickable.__init__(self)
        Erasable.__init__(self)
        self.height = height
        self.name = name
        self.face_up = face_up
        self.cards = []
        self.color = color
        self.all_decks.add(self)

    def __len__(self):
        return len(self.cards)

    def shuffle(self):
        random.shuffle(self.cards)
        debug('Deck {} shuffled: {}'.format(self, self.cards))

    def take_top(self):
        return self.cards.pop(0)

    def take_bottom(self):
        return self.cards.pop(-1)

    def take_all(self):
        cards = self.cards
        self.cards = []
        return cards

    def take_by_color(self, color, num_cards=None):
        indices = []
        for i in range(len(self.cards) - 1, -1, -1):
            if self.cards[i].color == color:
                indices.append(i)
            if num_cards is not None and len(indices) >= num_cards:
                break
        else:
            raise AssertionError(
                'Unable to find {} {} cards'.format(num_cards, color))
        return [self.cards.pop(i) for i in indices]

    @staticmethod
    def withdraw_card(card):
        for deck in Deck.all_decks:
            try:
                deck.cards.remove(card)
            except ValueError:
                pass

    def insert_top(self, card):
        try:
            for c in card:
                self.cards.insert(0, c)
        except TypeError:
            self.cards.insert(0, card)

    def insert_bottom(self, card):
        try:
            for c in card:
                self.cards.append(c)
        except TypeError:
            self.cards.append(card)

    def insert(self, index, card):
        self.cards.insert(index, card)

    def draw_static(self, canvas, x, y):
        fill = None
        if self.color is not None:
            fill = COLOR_DICT[self.color]
        canvas.create_rectangle(
            x, y,
            x + CARD_WIDTH, y + CARD_HEIGHT * (self.height + 1),
            outline=COLOR_DICT['k'],
            fill=fill,
            width=1)
        self.register_clickable(
            x, y, CARD_WIDTH, CARD_HEIGHT * (self.height + 1))

    def draw_dynamic(self, canvas, x, y):
        self.erase(canvas)
        for card in self.cards:
            card.deregister_clickable()
            card.erase(canvas)
        #
        if self.face_up:
            cards = self.cards[:self.height]
            for i, card in enumerate(cards):
                card.draw(canvas, x, y + CARD_HEIGHT * i)
            #
            num_extra = len(self) - len(cards)
            if num_extra > 0:
                self.erasable(canvas.create_text(
                    x + 4, y + self.height * CARD_HEIGHT,
                    anchor=tk.NW,
                    font=FONTS[16],
                    fill=COLOR_DICT['k'],
                    text='+{}'.format(num_extra)))
        else:
            self.erasable(canvas.create_text(
                x + 4, y + self.height * CARD_HEIGHT,
                anchor=tk.NW,
                font=FONTS[16],
                fill=COLOR_DICT['k'],
                text='{}'.format(len(self))))

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)
