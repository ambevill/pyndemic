import tkinter as tk
#
from color import COLOR_DICT
from font import FONTS
from clickable import Clickable


CARD_WIDTH = 100
CARD_HEIGHT = 18


class Card(Clickable):
    def __init__(self, text, color, rcolor):
        Clickable.__init__(self)
        self.text = text
        self.color = color
        self.rcolor = rcolor
        self.widgets = []

    def __str__(self):
        return self.text

    def __repr__(self):
        return self.text

    def draw(self, canvas, x, y):
        # erase the previous sprite
        self.erase(canvas)
        #
        # outline and background
        self.widgets.append(canvas.create_rectangle(
            x, y,
            x + CARD_WIDTH, y + CARD_HEIGHT,
            fill=COLOR_DICT['w'],
            outline=COLOR_DICT['k'],
            width=1))
        # obverse color
        self.widgets.append(canvas.create_rectangle(
            x + 2, y + 2,
            x + CARD_HEIGHT - 2, y + CARD_HEIGHT - 2,
            fill=COLOR_DICT[self.color],
            outline=None))
        # text
        self.widgets.append(canvas.create_text(
            x + CARD_HEIGHT, y + 2,
            anchor=tk.NW,
            fill=COLOR_DICT['k'],
            font=FONTS[16],
            text=self.text))
        # reverse color
        self.widgets.append(canvas.create_rectangle(
            x + CARD_WIDTH - 8, y,
            x + CARD_WIDTH, y + 8,
            fill=COLOR_DICT[self.rcolor]))
        #
        self.register_clickable(x, y, CARD_WIDTH, CARD_HEIGHT)

    def erase(self, canvas):
        while len(self.widgets) > 0:
            canvas.delete(self.widgets.pop())
        self.deregister_clickable()


class PlayerCard(Card):
    def __init__(self, text, color):
        Card.__init__(self, text, color, 'b')

    #def __str__(self):
    #    return self.text + ' Knowledge'

    def __repr__(self):
        return str(self)


class InfectionCard(Card):
    def __init__(self, city):
        Card.__init__(self, city.name, city.color, 'g')
        self.city = city

    #def __str__(self):
    #    return self.text + ' Infection'

    def __repr__(self):
        return str(self)
