import tkinter as tk
#
from output import info
from color import COLOR_DICT
from font import FONTS
from clickable import Clickable
from erasable import Erasable


SKULL = '\u2620'


class CubeCounter(Clickable, Erasable):
    WIDTH = 21
    UNCURED = 0
    CURED = 1
    ERADICATED = 2

    def __init__(self, color, bcolor='k', total=24):
        Clickable.__init__(self)
        Erasable.__init__(self)
        self.color = color
        self.bcolor = bcolor
        self.total = total
        self.status = self.UNCURED

    def increment(self, better=True):
        if self.color == 'w':
            return
        if better:
            self.status = min(self.status + 1, self.ERADICATED)
        else:
            self.status = max(self.status - 1, self.UNCURED)

    def status(self):
        return ('uncured', 'cured', 'eradicated')[self.status]

    def draw_static(self, canvas, x, y):
        if self.color == 'w':
            xl, xc, xr = x, x + self.WIDTH // 2, x + self.WIDTH
            yl, yc, yh = y + self.WIDTH, y + self.WIDTH // 2, y
            canvas.create_polygon(
                ((xl, yl), (xl, yc), (xc, yh), (xr, yc), (xr, yl)),
                fill=COLOR_DICT[self.color],
                outline=COLOR_DICT[self.bcolor],
                width=1)
        else:
            canvas.create_rectangle(
                x, y,
                x + self.WIDTH, y + self.WIDTH,
                fill=COLOR_DICT[self.color],
                outline=COLOR_DICT[self.bcolor],
                width=1)
        self.register_clickable(x, y, self.WIDTH, self.WIDTH)

    def draw_dynamic(self, canvas, x, y, num_used_dict):
        self.erase(canvas)
        remaining = self.total - num_used_dict[self.color]
        if remaining < 0 and self.color != 'w':
            remaining = SKULL
        else:
            remaining = str(remaining)
        #
        self.erasable(canvas.create_text(
            (x + self.WIDTH / 2., y + self.WIDTH / 2.),
            font=FONTS[16],
            text=remaining,
            fill=COLOR_DICT[self.bcolor]))
        if self.status >= self.CURED:
            self.erasable(canvas.create_line(
                (x, y),
                (x + self.WIDTH, y + self.WIDTH),
                fill=COLOR_DICT[self.bcolor],
                width=1))
        if self.status >= self.ERADICATED:
            self.erasable(canvas.create_line(
                (x + self.WIDTH, y),
                (x, y + self.WIDTH),
                fill=COLOR_DICT[self.bcolor],
                width=1))

    def __str__(self):
        return 'cure status {}'.format(self.color)


class RateCounter(Clickable, Erasable):
    RATES = (2, 2, 2, 3, 3, 4, 4)
    RADIUS = 9

    def __init__(self):
        Clickable.__init__(self)
        Erasable.__init__(self)
        self.count = 0
        self.centers = []

    def increment(self, num_positions=1):
        self.count += num_positions

    def get_current_rate(self):
        return self.RATES[self.count]

    def draw_static(self, canvas, x, y):
        self.centers = []
        for i, n in enumerate(self.RATES):
            canvas.create_text(
                (x + i * 16, y),
                font=FONTS[16],
                text=str(n),
                fill=COLOR_DICT['k'])
            self.centers.append((x + i * 16, y))
        self.register_clickable(
            x - self.RADIUS,
            y - self.RADIUS,
            2 * self.RADIUS + self.centers[-1][0] - self.centers[0][0],
            2 * self.RADIUS)

    def draw_dynamic(self, canvas, x, y):
        self.erase(canvas)
        center = self.centers[self.count]
        self.erasable(canvas.create_oval(
            center[0] - self.RADIUS, center[1] - self.RADIUS,
            center[0] + self.RADIUS, center[1] + self.RADIUS,
            outline=COLOR_DICT['k'],
            width=1))


class OutbreakCounter(Clickable, Erasable):
    allowed = 8

    def __init__(self):
        Clickable.__init__(self)
        Erasable.__init__(self)
        self.width, self.height = 0, 0
        self.count = 0

    def increment(self, num_outbreaks=1):
        self.count += num_outbreaks
        info(f"{OutbreakCounter.allowed - self.count} outbreaks remain before panic.")

    def draw_static(self, canvas, x, y):
        pass

    def draw_dynamic(self, canvas, x, y):
        self.erase(canvas)
        remaining = OutbreakCounter.allowed - self.count
        text = 'Outbreaks Before Panic: {}'.format(
            remaining if remaining > 0 else SKULL)
        text_item = canvas.create_text(
            (x, y),
            anchor=tk.NW,
            text=text,
            font=FONTS['20i'],
            fill=COLOR_DICT['k'])
        self.erasable(text_item)
        bbox = canvas.bbox(text_item)
        self.register_clickable(*bbox)
