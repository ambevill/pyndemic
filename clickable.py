import tkinter as tk


class Clickable(object):
    all_clickables = set()

    def __init__(self):
        object.__init__(self)

    def register_clickable(self, x, y, width, height):
        self.click_attributes = (x, y, x + width, y + height)
        self.all_clickables.add(self)

    def deregister_clickable(self):
        self.click_attributes = None
        try:
            self.all_clickables.remove(self)
        except KeyError:
            pass

    def clickable_area(self):
        return self.click_attributes[2] * self.click_attributes[3]

    def check_click(self, event):
        x1, y1, x2, y2 = self.click_attributes
        return x1 <= event.x < x2 and y1 <= event.y < y2

    @staticmethod
    def identify_click(event):
        clickables = []
        for clickable in Clickable.all_clickables:
            if clickable.check_click(event):
                clickables.append(clickable)
        if len(clickables) == 0:
            return None
        clickables.sort(key=lambda c: c.clickable_area())
        return clickables[0]
